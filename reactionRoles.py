import os
import discord
import logging
import jsonpickle
import json
from dataclasses import dataclass
logger = logging.getLogger('Reaction Roles')
logger.setLevel(logging.INFO)

@dataclass
class EmoteRoleChannel:
    Emote: str
    Role: int
    Channel: int
    
class reactionRoles:
    
    EmoteRoleChannels = []
    configPath = "EmoteRoles.json"
    
    def __init__(self):
        logger.info(f'Initializing Reaction Roles!')
        self.readConfig()
        
    
    def addEmoteRole(self, Role, Emote, Channel):
        logger.info(f'Emote: {Emote} Role: {Role} Channel: {Channel}')
        self.EmoteRoleChannels.append(EmoteRoleChannel(Emote, int(Role[3:-1]), Channel))
        self.saveConfig()
    
    def removeEmoteRole(self, Role, Emote, Channel):
        logger.info(f'Emote: {Emote} Role: {Role} Channel: {Channel}')
        self.EmoteRoleChannels.remove(EmoteRoleChannel(Emote, int(Role[3:-1]), Channel))
        self.saveConfig()
    
    async def on_raw_reaction_add(self, payload):
        logger.info(f'{payload.member} reacted with {payload.emoji}')
        if payload.emoji.name in [x.Emote for x in self.EmoteRoleChannels if x.Channel == payload.channel_id]:
            logger.info(f'{payload.member} getting Role!')
            await payload.member.add_roles(discord.utils.get(payload.member.guild.roles, id=[x.Role for x in self.EmoteRoleChannels if x.Channel == payload.channel_id and x.Emote == payload.emoji.name][0]))
    
                
    async def on_raw_reaction_remove(self, payload, client):
        guild = await client.fetch_guild(payload.guild_id)
        member = (await guild.query_members(user_ids=payload.user_id))[0]
        logger.info(f'{member} removed {payload.emoji} reaction')
        if payload.emoji.name in [x.Emote for x in self.EmoteRoleChannels if x.Channel == payload.channel_id]:
            logger.info(f'removing Role from {member}!')
            await member.remove_roles(discord.utils.get(member.guild.roles, id=[x.Role for x in self.EmoteRoleChannels if x.Channel == payload.channel_id and x.Emote == payload.emoji.name][0]))

    def readConfig(self):
        if os.path.exists(self.configPath):
            f = open(self.configPath, "r")
            try:
                self.EmoteRoleChannels = jsonpickle.decode(f.read())
            except json.decoder.JSONDecodeError:
                pass
        else:
            f = open(self.configPath, "w+")
        f.close()
    
    def saveConfig(self):
        f = open(self.configPath,"w")
        f.write(jsonpickle.encode(self.EmoteRoleChannels))
        f.close()