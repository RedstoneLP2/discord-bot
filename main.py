import discord
import logging
import os
import twitch_live_notification
import reactionRoles
from dotenv import load_dotenv
load_dotenv()
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
intents = discord.Intents.all()

class MyClient(discord.Client):
    notifier = None
    reactionRoles = None
    
    async def on_ready(self):
        print('Logged on as', self.user)
        if self.notifier == None:
            logger.info(f'Starting Notifier!')
            self.notifier = twitch_live_notification.notifier(self)
        if self.reactionRoles == None:
            logger.info(f'Starting ReactionRoles!')
            self.reactionRoles = reactionRoles.reactionRoles()

    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return
        if discord.utils.get(message.author.guild.roles, name='BotMaster') not in message.author.roles:
            await message.channel.send('YOU ARE NOT MY MASTER!')
            return
        if message.content.startswith('!twitchnotify'):
            if self.notifier == None:
                await message.channel.send(f'Starting Notifier!')
                logger.info(f'Starting Notifier!')
                self.notifier = twitch_live_notification.notifier(self)
                return
            #await message.channel.send(f'Notifier already exists!')
            match message.content.split(' ')[1]:
                case 'add':
                    self.notifier.addChannel(message.channel.id, message.content.split(' ')[2])
                    return
                case 'remove':
                    self.notifier.removeChannel(message.channel.id, message.content.split(' ')[2])
                    return
                case _:
                    return
        if message.content.startswith('!reactionrole'):
            match message.content.split(' ')[1]:
                case 'add':
                    self.reactionRoles.addEmoteRole(message.content.split(' ')[2],message.content.split(' ')[3],message.channel.id)
                    return
                case 'remove':
                    self.reactionRoles.removeEmoteRole(message.content.split(' ')[2],message.content.split(' ')[3],message.channel.id)
                    return
                case _:
                    return
                
    async def on_raw_reaction_add(self, payload):
        await self.reactionRoles.on_raw_reaction_add(payload)
                
    async def on_raw_reaction_remove(self, payload):
        await self.reactionRoles.on_raw_reaction_remove(payload, self)


client = MyClient(intents=intents)
client.run(os.getenv("TOKEN"))