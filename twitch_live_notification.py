import os
import json
import jsonpickle
import logging
import twitchAPI
import threading
import time
import asyncio
from dotenv import load_dotenv
from dataclasses import dataclass
logger = logging.getLogger('twitchnotify')
logger.setLevel(logging.INFO)
load_dotenv()

@dataclass
class UserChannelPair:
    User: str
    Channel: int
    ToMention: str

class notifier:
    def __init__(self, DiscordClient):
        self.discordClient = DiscordClient
        self.readConfig()
        logger.info(f'Initialized Notifier!')
        self.twitch = twitchAPI.Twitch(os.getenv("TWITCH_API_KEY"),os.getenv("TWITCH_API_SECRET"))
        self.startThread()
        
    def addChannel(self, DiscordChannelID, TwitchChannel, Mentions=[""]):
        self.UCP.append(UserChannelPair(TwitchChannel, DiscordChannelID, Mentions))
        logger.info(f'Added Twitch Channel {TwitchChannel}')
        self.saveConfig()

    def removeChannel(self, DiscordChannelID, TwitchChannel):
        for pair in self.UCP:
            if pair.User == TwitchChannel and pair.Channel == DiscordChannelID:
                self.UCP.remove(pair)
        self.saveConfig()
        
    def readConfig(self):
        if os.path.exists(self.configPath):
            f = open(self.configPath, "r")
            try:
                self.UCP = jsonpickle.decode(f.read())
            except json.decoder.JSONDecodeError:
                pass
        else:
            f = open(self.configPath, "w+")
        f.close()
    
    def saveConfig(self):
        f = open(self.configPath,"w")
        logger.info(self.UCP)
        f.write(jsonpickle.encode(self.UCP))
        f.close()
        
    def searchLiveChannels(self, Channels):
        if len(Channels) > 100:
            return #TODO: Figure out how to
        
        return self.twitch.get_streams(user_login=Channels)
        
    def startThread(self):
        x = threading.Thread(target=self.loop, args=())
        x.start()

    def loop(self,):
        while True:
            if len(self.UCP) == 0:
                logger.info(f'Sleeping for 5 Minutes')
                time.sleep(60*5)
                continue
            streamers = []
            for UserPair in self.UCP:
                streamers.append(UserPair.User)
            streamers = list(set(streamers))
            liveStreams = self.searchLiveChannels(streamers)
            self.CheckForLive(liveStreams)
            
            time.sleep(60)
        
        
    def CheckForLive(self, LiveStreams):
        #logger.info(LiveStreams["data"][0]["user_name"])
        live = []
        for stream in LiveStreams["data"]:
            if stream["type"] == "live":
                live.append(stream["user_name"])
        for streamer in self.UCP:
            logger.info(f'{streamer.User}')
            if streamer.User.lower() in map(str.lower, live):
                if streamer.User not in self.whoslive:
                    lowerList = list(map(str.lower, live))
                    index = lowerList.index(streamer.User.lower())
                    self.whoslive.append(streamer.User)
                    logger.info(f'{live[index]} is now live!')
                    self.discordClient.loop.create_task(self.discordClient.get_channel(streamer.Channel).send(f'{live[index]} is now Live! https://twitch.tv/{live[index]} {streamer.ToMention[0]}'))
    
    whoslive = []
    
    configPath = "UserChannelPairs.json"
    
    UCP = []
    
    twitch = None
    
    discordClient = None